import argparse
import cv2
import json
import os
import time
import numpy as np

from classification.linear_classification import GenerativeMLBiClassifier


def parse_args():
    parser = argparse.ArgumentParser(
        description='Run Generative Linear Classifier')
    parser.add_argument('data_dir')
    parser.add_argument('num_points_x', type=int)
    parser.add_argument('num_points_y', type=int)
    return parser.parse_args()


if __name__ == '__main__':
    args = parse_args()

    weight_data = {}
    processing_time = []
    for i in range(0, args.num_points_y):
        start = time.time()
        for j in range(0, args.num_points_x):
            point_key = '%s:%s' % (i, j)
            file_name = '%s.json' % point_key
            summary_dir = os.path.join(args.data_dir, 'summary')
            with open(os.path.join(summary_dir, file_name), 'r') as json_f:
                data = json.load(json_f)
            start_process = time.time()
            classifier = GenerativeMLBiClassifier(
                np.array(data['inputs']),
                np.array(data['truths'])
            )
            end_process = time.time()
            processing_time.append(end_process - start_process)
            weights, w_0 = classifier.get_weights()
            weight_data[point_key] = {
                'weights': weights.tolist(),
                'w_0': w_0.tolist()
            }
        end = time.time()
        print('Processed row %s, took %ss' % (i, end - start))
    weight_file_path = os.path.join(args.data_dir, 'weight_data.json')
    with open(weight_file_path, 'w') as weight_file:
        json.dump(weight_data, weight_file)
    processing_file_path = os.path.join(args.data_dir, 'processing_time.json')
    with open(processing_file_path, 'w') as processing_file:
        json.dump(processing_time, processing_file)