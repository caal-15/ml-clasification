import argparse
import json
import os


def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument('data_dir')
    return parser.parse_args()


if __name__ == '__main__':
    args = parse_args()

    processing_path = os.path.join(args.data_dir, 'processing_time.json')
    with open(processing_path, 'r') as processing_file:
        processing_data = json.load(processing_file)

    processing_time_total = 0
    for processing_time in processing_data:
        processing_time_total += processing_time
    processing_time_mean = processing_time_total / len(processing_data) * 1000
    print('Avg. Processing time per point: %sms' % processing_time_mean)
    print('Total Processing time for the model: %ss' % processing_time_total)

    stats_path = os.path.join(args.data_dir, 'stats.json')
    with open(stats_path, 'r') as stats_file:
        stats_data = json.load(stats_file)

    total_points = 0
    total_points_correct = 0
    total_points_wrong = 0
    total_points_per_img = stats_data[0]['correct'] + stats_data[0]['wrong']
    for stat in stats_data:
        total_points_correct += stat['correct']
        total_points_wrong += stat['wrong']
        total_points += (stat['correct'] + stat['wrong'])
    correct_percentage = total_points_correct / total_points * 100
    wrong_percentage = total_points_wrong / total_points * 100
    correct_per_img = total_points_correct / len(stats_data)
    wrong_per_img = total_points_wrong / len(stats_data)
    correct_percentage_pi = correct_per_img / total_points_per_img * 100
    wrong_percentage_pi = wrong_per_img / total_points_per_img * 100
    print('Total Points Used for Testing: %s' % total_points)
    print('Total Correct Percentage: %s' % correct_percentage)
    print('Total Wrong Percentage: %s' % wrong_percentage)
    print('Points per image: %s' % total_points_per_img)
    print('Avg. Correct per image: %s' % correct_per_img)
    print('Avg. Wrong per image: %s' % wrong_per_img)
    print('Avg. Correct percentage per image: %s' % correct_percentage_pi)
    print('Avg. Wrong percentage per image: %s' % wrong_percentage_pi)