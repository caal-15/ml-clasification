import argparse
import cv2
import json
import os
import time
import numpy as np

from classification.linear_classification import GenerativeMLBiClassifier


def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument('image_dir')
    parser.add_argument('data_dir')
    parser.add_argument('num_samples', type=int)
    parser.add_argument('img_size_x', type=int)
    parser.add_argument('img_size_y', type=int)
    return parser.parse_args()


if __name__ == '__main__':
    args = parse_args()
    classifiers = {}

    weight_path = os.path.join(args.data_dir, 'weight_data.json')
    with open(weight_path, 'r') as weight_file:
        weight_data = json.load(weight_file)

    for i in range(0, args.img_size_y):
        for j in range(0, args.img_size_x):
            data_key = '%s:%s' % (i, j)
            classifiers[data_key] = GenerativeMLBiClassifier()
            classifiers[data_key].set_weights(
                np.array(weight_data[data_key]['weights']),
                weight_data[data_key]['w_0']
            )

    sample_path = os.path.join(args.data_dir, 'samples.json')
    with open(sample_path, 'r') as sample_file:
        sample_data = json.load(sample_file)

    samples = sample_data['testing'][0:args.num_samples]
    stats_data = []
    input_dir = os.path.join(args.image_dir, 'input')
    truth_dir = os.path.join(args.image_dir, 'truth')
    generated_dir = os.path.join(args.data_dir, 'generated')
    if not os.path.exists(generated_dir):
        os.makedirs(generated_dir)
    for sample in samples:
        start = time.time()
        input_img = cv2.imread(
            os.path.join(input_dir, '%s.png' % sample))
        truth_img = cv2.imread(
            os.path.join(truth_dir, '%s.png' % sample))
        correct = 0
        wrong = 0
        generated_img = np.zeros((args.img_size_y, args.img_size_x, 3))
        for i in range(0, args.img_size_y):
            for j in range(0, args.img_size_x):
                data_key = '%s:%s' % (i, j)
                answer = classifiers[data_key].classify(input_img[i][j])
                bin_answer = 0
                if answer >= 0.5:
                    bin_answer = 1
                    generated_img[i][j] = np.array([255, 255, 255])
                truth = truth_img[i][j]
                bin_truth = 1
                if truth.all() == np.array([0, 0, 0]).all():
                    bin_truth = 0
                if bin_answer == bin_truth:
                    correct += 1
                else:
                    wrong += 1

        cv2.imwrite(
            os.path.join(generated_dir, '%s.png' % sample),
            generated_img
        )
        end = time.time()
        print(
            'Processed %s.png, correct: %s, wrong: %s, took %ss' %
            (sample, correct, wrong, end - start)
        )
        stats_data.append({'correct': correct, 'wrong': wrong})

    stats_path = os.path.join(args.data_dir, 'stats.json')
    with open(stats_path, 'w') as stats_file:
        json.dump(stats_data, stats_file)