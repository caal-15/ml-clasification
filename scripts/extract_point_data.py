import argparse
import cv2
import json
import os
import time

import numpy as np


def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument('image_dir')
    parser.add_argument('data_dir')
    parser.add_argument('start_image', type=int)
    parser.add_argument('end_image', type=int)
    parser.add_argument('sample_size', type=int)
    parser.add_argument('--remove_redundancy', action='store_true')
    return parser.parse_args()



if __name__ == '__main__':
    args = parse_args()
    all_indexes = np.random.permutation(
        np.arange(args.start_image, args.end_image))
    training_indexes = all_indexes[0:args.sample_size]
    testing_indexes = all_indexes[args.sample_size:]
    samples_path = os.path.join(args.data_dir, 'samples.json')
    with open(samples_path, 'w') as samples_file:
        json.dump(
            {
                'training': training_indexes.tolist(),
                'testing': testing_indexes.tolist()
            },
            samples_file
        )
    imgs = []
    truths = []

    input_dir = os.path.join(args.image_dir, 'input')
    truth_dir = os.path.join(args.image_dir, 'truth')
    for i in training_indexes:
        img_path = os.path.join(input_dir, '%s.png' % i)
        truth_path = os.path.join(truth_dir, '%s.png' % i)
        imgs.append(cv2.imread(img_path))
        truths.append(cv2.imread(truth_path))

    height, width = imgs[0].shape[:2]
    summary_dir = os.path.join(args.data_dir, 'summary')
    if not os.path.exists(summary_dir):
        os.makedirs(summary_dir)
    for i in range(0, height):
        start = time.time()
        for j in range(0, width):
            point_key = '%s:%s' % (i, j)
            cache = {}
            point_data = {'inputs': [], 'truths': []}
            for k in range(0, len(imgs)):
                point = imgs[k][i][j]
                truth = truths[k][i][j]
                truth_bin = 1
                if truth.all() == np.array([0, 0, 0]).all():
                    truth_bin = 0
                dict_key = '%s:%s' % (point, truth_bin)
                if not cache.get(dict_key, None) or not args.remove_redundancy:
                    if args.remove_redundancy:
                        cache[dict_key] = True
                    point_data['inputs'].append(point.tolist())
                    point_data['truths'].append(truth_bin)
            point_file_path = os.path.join(summary_dir, '%s.json' % point_key)
            with open(point_file_path, 'w') as point_file:
                json.dump(point_data, point_file)
        end = time.time()
        print('Processed row: %s, took %ss' % (i, end - start))
