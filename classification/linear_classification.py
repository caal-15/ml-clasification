import numpy as np


def sigmoid(x):
    return 1 / (1 + np.exp(-x))


class WeightsUnsetException(Exception):
    def __init__(self):
        super().__init__(
            'Weights have not been set, train the model or set them manually')


class GenerativeMLBiClassifier():

    def __init__(self, training_x=None, training_t=None):
        if training_x is not None and training_t is not None:
            self.train(training_x, training_t)
        else:
            self.weights = None
            self.w_0 = None

    def classify(self, new_x):
        if self.weights is not None and self.w_0 is not None:
            return sigmoid(self.weights.dot(new_x) + self.w_0)
        else:
            raise WeightsUnsetException

    def set_weights(self, weights, w_0):
        self.weights = weights
        self.w_0 = w_0

    def get_weights(self):
        return (self.weights, self.w_0)


    def train(self, training_x, training_t):
        total = len(training_x)
        c1_tot = np.sum(training_t)
        c2_tot = total - c1_tot
        dim = len(training_x[0])

        pi = c1_tot / total
        t_reshaped = training_t.reshape((1, total)).T

        mu_1 = np.zeros((dim))
        S_1 = np.zeros((dim, dim))
        if c1_tot > 0:
            mu_1 = np.sum(np.multiply(t_reshaped, training_x), axis=0) / c1_tot
            mu_1_mat = np.tile(mu_1, total).reshape((total, dim))
            err_mu_1 = training_x - mu_1_mat
            sq_err_mu_1 = np.einsum('ij,ik->ijk', err_mu_1, err_mu_1)
            sq_err_c1 = np.einsum('ij,ijk->ijk', t_reshaped, sq_err_mu_1)
            S_1 = np.sum(sq_err_c1, axis=0) / c1_tot

        mu_2 = np.zeros((dim))
        S_2 = np.zeros((dim, dim))
        if c2_tot > 0:
            mu_2 = np.sum(np.multiply(1 - t_reshaped, training_x), axis=0) / c2_tot
            mu_2_mat = np.tile(mu_2, total).reshape((total, dim))
            err_mu_2 = training_x - mu_2_mat
            sq_err_mu_2 = np.einsum('ij,ik->ijk', err_mu_2, err_mu_2)
            sq_err_c2 = np.einsum('ij,ijk->ijk', 1 - t_reshaped, sq_err_mu_2)
            S_2 = np.sum(sq_err_c2, axis=0) / c2_tot

        Sigma = ((c1_tot / total) * S_1) + ((c2_tot / total) * S_2)
        Sigma_inv = np.linalg.inv(Sigma + (np.eye(dim) * 1e-6))

        self.weights = Sigma_inv.dot(mu_1 - mu_2)
        w_0 = -0.5 * mu_1.T.dot(Sigma_inv).dot(mu_1)
        w_0 += 0.5 * mu_2.T.dot(Sigma_inv).dot(mu_2)
        w_0 += np.log(pi / (1 - pi))
        self.w_0 = w_0