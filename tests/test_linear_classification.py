import unittest
import numpy as np

from classification.linear_classification import GenerativeMLBiClassifier


class MLBiClassifierTestCase(unittest.TestCase):

    def test_basic_classification_or(self):
        x = np.array([
            [1, 1],
            [1, 0],
            [0, 1],
            [0, 0]
        ])
        t = np.array([1, 1, 1, 0])

        classifier = GenerativeMLBiClassifier(x, t)
        for test_point in x:
            if test_point[0] or test_point[1]:
                self.assertLessEqual(0.9, classifier.classify(test_point))
            else:
                self.assertGreaterEqual(0.1, classifier.classify(test_point))

    def test_basic_classification_and(self):
        x = np.array([
            [1, 1],
            [1, 0],
            [0, 1],
            [0, 0]
        ])
        t = np.array([1, 0, 0, 0])

        classifier = GenerativeMLBiClassifier(x, t)
        for test_point in x:
            if test_point[0] and test_point[1]:
                self.assertLessEqual(0.9, classifier.classify(test_point))
            else:
                self.assertGreaterEqual(0.1, classifier.classify(test_point))